//http://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&APPID=a16d021d1e17bdb2c8e6d917744ca3a8

const key = "a16d021d1e17bdb2c8e6d917744ca3a8";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
//    console.log(response);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error('Error Status: ' + response.status);
    }
}

//getForecast()
//    .then(data => console.log(data))
//    .catch(err => console.log(err));